<?php

$start = $_POST["start"];
$end = $_POST["end"];
$cookie = $_COOKIE["Previous"];

if(!isset($cookie)){
	$cookie = "none";
}

if(isset($start) && isset($end)){
	if(!empty($start) && !empty($end)){
		if(is_numeric($start) && is_numeric($end)){
			if($start <= $end){

				$rand = rand($start, $end);

				$cookie = $rand;

				$exp = time()+86400;

				if(isset($cookie)){
					setcookie("Previous", $rand, $exp);
				}
			}else{
				$error = "First number must be smaller than second number!";
			}
		}else{
			$error = "Only numbers can be entered!";
		}
	}else{
		$error = "Please enter both numbers!";
	}
}

?>

<!-- Author: Mantas Salasevicius -->
<!-- Author website: https://www.mantas.dev -->
<!-- Author email: mantas.4nex@gmail.com -->
<!-- Date: 2016 -->

<!DOCTYPE html>
<html lang='en'>

<head>
	<meta name='author' content='Mantas Salasevicius' />
	<meta name='keywords' content='' />
	<meta name='description' content='' />
	<meta charset='utf-8' />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=yes" />
  <title>Random number generator</title>
  <link href="reset.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">
</head>

<body>
	<div class='content'>
	<h1>Generate random number</h1>
	</div>

	<div class='content'>
	<br>

		<form action="index.php" method="POST">
			<input type="text" name="start" class="number" /> to
			<input type="text" name="end" class="number" /> =
			<input type="submit" value="GENERATE" class='submit' />
		</form>

		<div class='display'><?php echo $rand; ?></div>
		<h2><?php echo $error; ?></h2>
		<h1>Previous number was <?php echo $cookie; ?></h1>
	</div>
</body>

</html>
