# PHP random number generator
Screenshots:
* [Screenshot of index page](screenshot-index.png)

## Built With

PHP

## Authors

* **Mantas Salasevicius** - *Developer* - [Portfolio](https://mantas.dev), [Gitlab](https://gitlab.com/mantas.dev)

## Date of development

2016

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details